#!/usr/bin/python3.4
#
#     simplify_script.py
#
# remove diacritics and non-isolated hamzas, and change alif maqsuras to yas
#
# Dependencies:
#   * arabic_chars.py
#
# example of usage:
#   $ cat ../../../corpus_quran/data/processed/quranic_corpus.json | jq -r .[].form | python simplify_script.py
#
###############################################################################################################

from re import sub
from sys import stdin,stdout,stderr,exit
from argparse import ArgumentParser,FileType
from itertools import zip_longest


# try relative path, if not working try absolute path
try:
    from arabic_chars import PUNCTUATION, OTHER_SYMBOLS, \
                             ARABIC_DIACRITICS_VOWELS, QURANIC_EXTREME_REMOVE

except ImportError:
    
    from imp import load_source
    arabic_chars = load_source('arabic_chars', '/home/alicia/COBHUNI/development/corpus/sources/altafsir/processing/altafsir_extractor/gimme_verse_quran/arabic_chars.py')
    
    #ARABIC_SPECIAL_CHARS = arabic_chars.ARABIC_SPECIAL_CHARS
    PUNCTUATION = arabic_chars.PUNCTUATION
    OTHER_SYMBOLS = arabic_chars.OTHER_SYMBOLS
    ARABIC_DIACRITICS_VOWELS = arabic_chars.ARABIC_DIACRITICS_VOWELS

    QURANIC_EXTREME_REMOVE = arabic_chars.QURANIC_EXTREME_REMOVE

#########################################################################

#
# functions
#

def simplifyscript(text, **kwargs):
    """Simplifies Arabic script by removing diacritics and merging shapes of ambiguous letters.

    Args:
        text (iterable): Collection containing text.
        keep_vowels (bool): Flag to remove or not vowel diacritics.
        keep_hamza (bool): Flag to remove or not hamza discritic.
        keep_alif_maqsura (bool): Flag to convert or not alif maqsura into ya.

    Yields:
        string: simplified lines of text

    """

    global ARABIC_DIACRITICS_VOWELS

    # parse params
    keep_vowels = kwargs.pop('keep_vowels', False)
    keep_hamza = kwargs.pop('keep_hamza', False)
    keep_alif_maqsura = kwargs.pop('keep_alif_maqsura', False)

    assert len(kwargs) == 0, "unrecognized params passed in: %s" % ",".join(kwargs.keys())

    # process input
    for line in filter(None, (l.strip() for l in text)):

        # remove vowel diacritics
        if not keep_vowels:
            for char in ARABIC_DIACRITICS_VOWELS:
                line = line.replace(char,'')

        # remove hamzas from semiconsonant supporting it
        if not keep_hamza:
            line = sub('[أإ]', 'ا', line)
            line = line.replace('ؤ','و')
            line = line.replace('ئ','ي')

        # convert alif maqsuras into ya
        if not keep_alif_maqsura:
            line = line.replace('ى','ي')
            
        # remove special chars and punctuation
        for char in CHARS_TO_REMOVE:
            line = line.replace(char,'')

        # normalise spaces
        yield sub(' +', ' ', line.strip())


def simplifyscript_extreme(text):
    """Simplifies Arabic script to a extreme form by removing all diacritics, punctuation and several letters.
    Meant for quranic matches.

    Args:
        text (iterable): Collection containing text.

    Yields:
        str: simplified lines of text
    """

    global QURANIC_EXTREME_REMOVE

    for line in filter(None, (l.strip() for l in text)):

        # convert newline within aya text into space
        line = line.replace('\n',' ')
    
        # ad-hoc conversions for altafsir typos
        line = line.replace('هَا هُنَا','هَاهُنَا') #(69:35:4-(5))
        line = line.replace('بَعْدَ مَا', 'بَعْدَمَا')
        line = line.replace('هَا أَنتُمْ', 'هَاأَنتُمْ')
        line = line.replace('أَمَّا ذَا', 'أَمَّاذَا')
        line = line.replace('مَالِيَ', 'مَا لِيَ')
        line = line.replace('نُنجِـي', 'نج')
        line = line.replace('بَصْۜطَةً','بَصْطَةً')  # exception to sad + small sin into sin conversion
    
        # convert Sad + small sin letter above (0x6dc) into sin
        line = line.replace('صۜ','س')
    
        # remove alifs, diacritics, hamzas and yas
        line = sub('[ءؤئإأآاٱىيو]', '', line)
    
        # remove diacritics, special chars and punctuation    
        for char in QURANIC_EXTREME_REMOVE:
            line = line.replace(char,'')
    
        # reduce 3 equal chars to 2
        line = sub(r'(.)\1\1', r'\1\1', line)
    
        # reduce all groups of lams into one
        line = sub('(ل+)', 'ل', line)
    
        #NOTE: typo in altafsir: (40:6:3) is written "كَلِمَةُ" instead of "كَلِمَتُ"
        line = line.replace('كلمة','كلمت')
    
        # convert all ta marbutas in tas:
        line = line.replace('ة','ت')        
        
        # normalise spaces
        yield sub(' +', ' ', line.strip())


#########################################################################

#
# main
#

if __name__ == '__main__':
    
    parser = ArgumentParser(description='simplify Arabic script by removing diacritics and merging shapes of ambiguous letters')
    parser.add_argument('input_text', nargs='?', type=FileType('r'), default=stdin, help='input stream with text')
    parser.add_argument('output_text', nargs='?', type=FileType('w'), default=stdout, help='output stream for modified text')
    parser.add_argument('--keep_vowels', '-v', action='store_true', help='do not remove vowels nor sukun')
    parser.add_argument('--keep_hamza', '-z', action='store_true', help='do not remove hamza diacritic')
    parser.add_argument('--keep_alif_maq', '-m', action='store_true', help='do not convert alif maqsura into ya')
    parser.add_argument('--quranic', action='store_true', help='extreme simplification (ignores all other flags)')
    
    args = parser.parse_args()

    # normal simplification
    if not args.quranic:

        # prepare data
        CHARS_TO_REMOVE = {}
        for d in (PUNCTUATION, OTHER_SYMBOLS):
            CHARS_TO_REMOVE.update(d)

        for line in simplifyscript(args.input_text, keep_vowels=args.keep_vowels,
                                                    keep_hamza=args.keep_hamza,
                                                    keep_alif_maqsura=args.keep_alif_maq):
            
            print(line, file=args.output_text)

    # quranic simplification
    else:

        for line in simplifyscript_extreme(args.input_text):
            print(line, file=args.output_text)

