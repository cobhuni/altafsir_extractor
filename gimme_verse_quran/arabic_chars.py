#!/usr/bin/python3.4
#
#     arabic_cahrs.py
# 
# dicts with arabic characters and punctuation
#
###########################################################################################################

ARABIC_CONSONANTS = {
                      'ء' : 'hamza' ,
                      'آ' : 'madda' ,
                      'أ' : 'hamza above alif' ,
                      'إ' : 'hamza below alif' ,
                      'ؤ' : 'hamza above waw' ,
                      'ئ' : 'hamza above ya' ,
                      'ٱ' : 'alif wasla' ,
                      'ا' : 'alif' ,
                      'ب' : 'ba' ,
                      'ة' : 'ta marbuta' ,
                      'ت' : 'ta' ,
                      'ث' : 'tha' ,
                      'ج' : 'jim' ,
                      'ح' : 'Ha' ,
                      'خ' : 'kha' ,
                      'د' : 'dal' ,
                      'ذ' : 'dhal' ,
                      'ر' : 'ra' ,
                      'ز' : 'zay' ,
                      'س' : 'sin' ,
                      'ش' : 'shin' ,
                      'ض' : 'DaD' ,
                      'ص' : 'SaD' ,
                      'ط' : 'Ta' ,
                      'ظ' : 'THA' ,
                      'ع' : '\'ayn' ,
                      'غ' : 'ghayn' ,
                      'ف' : 'fa' ,
                      'ق' : 'qaf' ,
                      'ك' : 'kaf' ,
                      'ل' : 'lam' ,
                      'م' : 'mim' ,
                      'ن' : 'nun' ,
                      'ه' : 'ha' ,
                      'و' : 'waw' ,
                      'ى' : 'alif maqsura' ,
                      'ي' : 'ya' ,
}

OTHER_ARABIC_SCRIPT_CONSONANTS = {
                    'ے' : 'arabic letter yeh barree (urdu)' ,  # 0x6d2
}

# quranic
ARABIC_DIACRITIC_CONSONANTS = { 
                  'ٓ' : 'isolated madda above [RARE]' ,
                  'ٔ' : 'isolated hamza above [RARE]' ,
                  'ۜ' : 'small sin letter above [RARE]' ,
                  'ۢ' : 'small mim letter above [RARE]' ,
                  'ۣ' : 'small sin letter below [RARE]' ,
                  'ۥ'  : 'small waw [RARE]' ,
                  'ۦ'  : 'small yeh marduda [RARE]' ,
                  'ۨ' : 'small nun above [RARE]' ,
                  '۪' : 'empty centre stop below [RARE]' ,
                  '۫' : 'empty centre stop above [RARE]' ,
                  'ۭ' : 'small mim below [RARE]' ,
                  'ۤ'  : 'small high madda [RARE]' ,     # U+6e4 (utf8: 0xDB 0xA4)
                  'ٰ'  : 'alif khanjariyya' ,            # U+670 (utf8: 0xEA 0x99 0xB0)
                  'ۡ' : 'small high dotless head of khah (used in some qurans to mark absence of vowel)' , # U+6e1
                  'ٴ' : 'arabic letter high hamza' ,  # U+674
}

# vowels and shadda
ARABIC_DIACRITICS_VOWELS = { 
                  'َ' : 'fatha' ,
                  'ً' : 'double fatha' ,
                  'ُ' : 'damma' ,
                  'ٌ' : 'double damma' ,
                  'ِ' : 'kasra' ,
                  'ٍ' : 'double kasra' ,
                  'ّ' : 'shadda' ,
                  'ْ' : 'sukun' ,
}

# quranic
ARABIC_DIACRITIC_VOWELS_EXTENSION = { 
                  '۟' : 'small sukun [RARE]' ,
                  '۠' : 'small rectangular zero above [RARE]' ,
                  '۬' : 'rounded stop with filled centre above [RARE]' ,
}


PUNCTUATION = {
                '?'  : 'Western question mark' ,
                '؟'  : 'Arabic question mark' ,
                '!'  : 'exclamation mark' ,
                '"'  : '(double) quotation mark' ,
                '‘'  : 'left single quotation mark' , # U+2018
                '”'  : 'right double quotation mark' , # U+206F
                "“"  : 'left double quotation mark' , # 0x201c
                "„"  : 'double low-9 quotation mark' , # 0x201e
                "‟"  : 'double high-reversed-9 quotation mark' , # 0x201f
                "″"  : 'double prime' , # 0x2033
                '«'  : 'opening angle quote' ,
                '»'  : 'closing angle quote' ,
                '\'' : 'apostrophe/single quotation mark' ,
                ','  : 'comma' ,
                '،'  : 'Arabic comma' ,
                ':'  : 'colon' ,
                ';'  : 'semicolon' ,
                '؛'  : 'Arabic semicolon' ,
                '.'  : 'dot' ,
                '-'  : 'hyphen-minus' ,
                '('  : 'opening parenthesis/round blacket' ,
                ')'  : 'closing parenthesis/round blacket' ,
                '{'  : 'opening brace/curly bracket' ,
                '}'  : 'closing brace/curly bracket' ,
                '['  : 'opening (square) bracket' ,
                ']'  : 'closing (square) bracket' ,
                '='  : 'equal sign' ,
                '<'  : 'minor than sign' ,
                '>'  : 'major than sign' ,
                '/'  : 'slash' ,
                '\\' : 'backslash' ,
                '¤'  : 'currency sign' ,
                '*'  : 'asterisk' ,
                '#'  : 'hash' ,
                '@'  : 'at-sign' ,
                '_'  : 'underscore' ,
                '&'  : 'ampersand' ,
                '%'  : 'percentage sign' ,
                '❊'  : 'eight teardrop-spoked propeller asterisk' ,
                '…'  : 'horizontal ellipsis' , 
                '®'  : 'registered trademark symbol' , 
                '⎨'  : 'left curly bracket middle piece' ,  # 0x23a8
                '⎬'  : 'right curly bracket middle piece' , # 0x23AC
}

NUMBERS = {
            '0' : 'Western Arabic numeral zero' , 
            '1' : 'Western Arabic numeral one' , 
            '2' : 'Western Arabic numeral two' , 
            '3' : 'Western Arabic numeral three' , 
            '4' : 'Western Arabic numeral four' , 
            '5' : 'Western Arabic numeral five' , 
            '6' : 'Western Arabic numeral six' , 
            '7' : 'Western Arabic numeral seven' , 
            '8' : 'Western Arabic numeral eight' , 
            '9' : 'Western Arabic numeral nine' , 
            '٠' : 'Eastern Arabic numeral zero' , 
            '١' : 'Eastern Arabic numeral one' , 
            '٢' : 'Eastern Arabic numeral two' , 
            '٣' : 'Eastern Arabic numeral three' , 
            '٤' : 'Eastern Arabic numeral four' , 
            '٥' : 'Eastern Arabic numeral five' , 
            '٦' : 'Eastern Arabic numeral six' , 
            '٧' : 'Eastern Arabic numeral seven' , 
            '٨' : 'Eastern Arabic numeral eight' , 
            '٩' : 'Eastern Arabic numeral nine' , 
}

LATIN_LETTERS = {
                  'a' : 'Lower letter a' ,
                  'B' : 'Capital letter B' ,
                  'C' : 'Capital letter C' , # 0x43
                  'E' : 'Capital letter E' , # 0x45
                  'G' : 'Capital letter G' , # 0x47
                  'H' : 'Capital letter H' , # 0x48
                  'M' : 'Capital letter M' , # 0x4d
                  'N' : 'Capital letter N' , # 0x4e
                  'O' : 'Capital letter O' , # 0x4f
                  'P' : 'Capital letter P' , # 0x50
                  'p' : 'Lower letter p' ,
                  'R' : 'Capital letter R' ,
                  'S' : 'capital letter S' , # 0x53
                  'T' : 'capital letter T' , # 0x54
                  'ʈ' : 't with retroflex hook' ,
                  'U' : 'Capital letter U' , # 0x55
                  'W' : 'Capital letter W' ,
                  'X' : 'capital letter X' , # 0x58
                  'Y' : 'capital letter Y' , # 0x59
                  'Z' : 'capital letter Z' , # 0x5a
}


OTHER_SYMBOLS = {
                'ـ' : 'tatweel/kashida' ,
                chr(0x200d) : 'Zero-width joiner (ZWJ)' ,      # utf8: 0xE2 0x80 0x8D
                chr(0x200c) : 'zero-width non-joiner (ZWNJ)' , # utfg: 0xE2 0x80 0x8C
                '' : 'U+F020 is not a valid unicode character' , # 0xf020
                "‎" : 'left-to-right mark' , # 0x200e  
                "‏" : 'right-to-left mark' , # 0x200f  
                "‫" : 'right-to-left embedding' , # 0x202b  
                "‬" : 'pop directional formatting' , # 0x202c  
                "﻿" : 'zero width no-break space' , # 0xfeff  
}


# merge all dicts of known chars
KNOWN_CHARS = {}

for d in (ARABIC_CONSONANTS, OTHER_ARABIC_SCRIPT_CONSONANTS, ARABIC_DIACRITIC_CONSONANTS, ARABIC_DIACRITICS_VOWELS,
          ARABIC_DIACRITIC_VOWELS_EXTENSION, PUNCTUATION, NUMBERS, LATIN_LETTERS, OTHER_SYMBOLS):

    KNOWN_CHARS.update(d)


# list of characters to be removed for an extreme reduction of script, for quranic matches
# NOTE unstable, in general not recommended. it has been used only for matching altafsir downloaded files with the ayahs being commented
QURANIC_EXTREME_REMOVE = 'ـۣٓٔۜ۟۠ۢۥۦ۪ۭۨ۫۬ۤ?؟!"«»\,،:;؛.-(){}[]=<>/¤*%❊…ًٌٍَُِّْٰ'
