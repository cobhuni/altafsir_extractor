#!/usr/bin/python3.4
# 
#    gimme_verse_index.py
#
# Given a text returns the number of sura and aya that it belongs or "-1 -1" if not found.
#
# Dependencies:
#   * simplify_script.py
#   * quranic_corpus_joined.json
#
# Note:
#   More than one references may be found, specially if flag --similar is set to True
#
# Example:
#   $ echo <text> | python gimme_verse_index.py
#
#############################################################################################

from sys import argv,stdin,exit
from os import path
from json import load
from argparse import ArgumentParser
from imp import load_source
from collections import defaultdict

#
# constants and relative imports
#

# current directory
THISPATH = path.dirname(path.realpath(argv[0]))

simplify = load_source('simplify_script',
                       '/home/alicia/COBHUNI/development/corpus/sources/altafsir/processing/altafsir_extractor/gimme_verse_quran/simplify_script.py').simplifyscript_extreme

QURAN_CORPUS = '/home/alicia/COBHUNI/development/corpus/sources/altafsir/processing/altafsir_extractor/gimme_verse_quran/quranic_corpus_joined.json'

#DEPRECATED
#hashQuran = dict((x['text'].strip(),(x['index']['sura'],x['index']['aya'])) for x in load(open(QURAN_CORPUS)))

# { text : [(isura,iaya), ...] }
with open(QURAN_CORPUS) as inf:
    hashQuran = defaultdict(list)
    for entry in load(inf):
        hashQuran[entry['text'].strip()].append((entry['index']['sura'],entry['index']['aya']))
    
# for not exact searches, simplify all verses
with open(QURAN_CORPUS) as inf:
    hashQuran_simplified = defaultdict(list)
    for entry in load(inf):
        hashQuran_simplified[next(simplify((entry['text'],))).strip()].append((entry['index']['sura'],entry['index']['aya']))

#
# functions
#

def getIndex(mytext, exact_script=True, subtext=False):
    """Yield the sura,aya indexes of a given text

    Args:
        mytext (str): input text to search
        exact_script (bool): flag indicating if the match should be exact or the texts can be simplified
        subtext (bool): flag indicating if mytext can be a subtext of the verse (true), or must be the same complete text
        corpus (dict): json object with all quran

    Yields:
        int,int: sura and aya indexes, -1 -1 if text not found

    """

    global hashQuran
    global hashQuran_reversed

    mytext = mytext.strip()

    # if similar match, then
    if not exact_script:

        # simplify input text
        mytext = next(simplify((mytext,))).strip()
    
    
    NOT_FOUND = True

    # if similar match
    if not exact_script:
        hashQuran = hashQuran_simplified


    # subtext comparison, we cannot escape loop!! >:||
    if subtext:

        for text in hashQuran:
    
            if mytext in text:
    
                NOT_FOUND = False
                for ref in hashQuran[text]:
                    yield ref[0], ref[1]
        
        
    # full text comparison, hash power!!! arggghhhhh
    else:

        if mytext in hashQuran:
    
            NOT_FOUND = False
            for ref in hashQuran[mytext]:
                yield ref[0], ref[1]
    
    if NOT_FOUND:
        yield -1, -1


#
# main
#

if __name__ == '__main__':

    # parse args
    parser = ArgumentParser(description='given some text from stdin prints into stdout numbers of sura and aya')
    parser.add_argument('--similar','-s', action='store_true', help='consider a similar match instead of exact')
    parser.add_argument('--subtext','-p', action='store_true', help='the text may be a subtext from the original verse')
    args = parser.parse_args()

    for sura,aya in getIndex(stdin.read().strip(), exact_script=not args.similar, subtext=args.subtext):

        print(sura, aya)
