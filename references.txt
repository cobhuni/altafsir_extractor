# sura:aya references to download commentaries from altafsir.com
################################################################
#
#procreation:
32:8    # mā'mahīn, „mean water“
77:20   # mā'mahīn, „mean water“
86:6    # mā'dāfiq, „gushing water“
#
# creation:
21:30   # every living being   #FIXME perhaps should be left out
24:45   # animals
25:54   # humans
16:4    # nuṭfa (lit. a drop)
18:37   # nuṭfa
22:5    # nuṭfa / ʿalaq(a) / mudgha (lump of flesh)
23:13   # nuṭfa
35:11   # nuṭfa
36:77   # nuṭfa
40:67   # nuṭfa / ʿalaq(a)
53:46   # nuṭfa
75:37   # nuṭfa
76:2    # nuṭfa
80:19   # nuṭfa
96:2    # ʿalaq(a) (blod­-clot)
23:14   # ʿalaq(a) / mudgha (lump of flesh)
75:38   # ʿalaq(a)
#
# duration of pregnancy plus weaning
46:15   # thirty months
2:233   # maximum duration of weaning in others
31:14   # two years    #FIXME perhaps should be left out
13:8
#
2:234   # email from 18 January 2016 at 17:21
11:105  # email from 15 March 2016 at 08:48