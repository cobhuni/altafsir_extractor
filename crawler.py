#!/usr/bin/python3.4

#     crawler.py
#
# web crawler for altafasir.com
# 
# usage:
#   $ nohup python crawler.py references.txt ../../data/ori/ &
#
# dependencies:
#     util.py
#                                                                             # 
# Notes:                                                                     #   احفظ الرمز يا كبيكج
# * web to crawl, www.altafsir.com, does not include robots.txt             #
# * Web to check user agent: http://whatsmyuseragent.com/
# * web to view xml: http://codebeautify.org/
# * beautiful soup guide: http://omz-software.com/pythonista/docs/ios/beautifulsoup_guide.html
#
####################################################################################

from util import madhabTafsirTable,loadReferences
from sys import stdin,stdout,exit
from os.path import join as joinpath
from urllib.request import urlopen,Request
from urllib import parse
from bs4 import BeautifulSoup
from argparse import ArgumentParser,FileType
from time import time,sleep
from random import uniform

parser = ArgumentParser(description='download all commentaries of sura:aya pairs from altafsir.com')
parser.add_argument('reference_file', nargs='?', type=FileType('r'), default=stdin, help='list of sura:aya pairs')
parser.add_argument('directory_name', action='store', help='directory to store all html files')
parser.add_argument('--debug', action='store_true', help='print each query data to stderr')

args = parser.parse_args()

# ────────────────────────────────────────────────────
# constants and configuration
# ────────────────────────────────────────────────────

SLEEP_MIN = 0.986
SLEEP_MAX = 11.52

URL = 'http://altafsir.com/Tafasir.asp'

VARS = { 'tMadhNo'     : '0',     # 1-10
         'tTafsirNo'   : '0',     # 1-105 (depends on tMadhNo chosen)
         'tSoraNo'     : '0',     # 1-114
         'tAyahNo'     : '0',     # 1-286 (depends on the tSoraNo chosen)
         'tDisplay'    : 'yes',   # always yes
         'Page'        : '1',     # number undetermined, should be updated after first query
         'UserProfile' : '0',     # always 0
         'LanguageId'  : '1'  }   # 1:Arabic, 2:English

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36'
headers = {'User-Agent' : USER_AGENT}

# ────────────────────────────────────────────────────
# functions
# ────────────────────────────────────────────────────

def addData2Url(url,data):
    '''Add variables to url and return string'''
    datastr='&'.join(['%s=%s' % (k,v) for k,v in  data.items()])
    return '%s?%s' % (url,datastr)

def combinations(refs):
    '''Generates all combinations of variables for URL requests and return tuple of strings.'''
    for madhn,listtafsir in madhabTafsirTable.items():
        for tafsirn in listtafsir:
            for soran,ayan in refs:
                yield str(madhn),str(tafsirn),soran,ayan

def requestURL(url,data):
    '''Request URL and return html string.'''
    fullurl = addData2Url(url,data)
    req = Request(fullurl, headers=headers)
    resp = urlopen(req)
    return BeautifulSoup(resp.read())

#DEPRECATED unexpected behaviour using the dictionary in a POST request, better to use a GET request (above)
#           all requests always yiled Page=1 regarless how we have set this variable
#def requestURL():
#    info = parse.urlencode(VARS)
#    info = info.encode('utf-8') # encode data in bytes
#    req = Request(URL, info, headers=headers)
#    resp = urlopen(req)
#    return BeautifulSoup(resp.read())

def saveRetrivedURL(soup,madhn,tafsirn,soran,ayan,pagen):
    '''Saves html string into a file.'''
    # check query is not empty
    if soup.body.div.find('div', id='SearchResults'):
        fname = 'altafsir-%s-%s-%s-%s-%s-%d.html' % (madhn,tafsirn,soran,ayan,pagen,int(time()))
        fpath = joinpath(args.directory_name, fname)    
        with open(fpath,'w') as outf:
            print(soup.prettify(), file=outf)
    return

# ────────────────────────────────────────────────────
# main
# ────────────────────────────────────────────────────

# load references
refs = loadReferences(args.reference_file)

for madhn,tafsirn,soran,ayan in combinations(refs):

    VARS['tMadhNo']   = madhn
    VARS['tTafsirNo'] = tafsirn
    VARS['tSoraNo']   = soran
    VARS['tAyahNo']   = ayan
    VARS['Page']      = '1'

    # open and read url
    respData = requestURL(URL,VARS)

    if args.debug:
        print('processing query madhn=%s tafsirn=%s soran=%s ayan=%s pagen=%s' % (madhn,tafsirn,soran,ayan,VARS['Page']))

    # save html of query for page 1
    saveRetrivedURL(respData,madhn,tafsirn,soran,ayan,'1')
    sleep(uniform(SLEEP_MIN, SLEEP_MAX))  # wait random time to do each query

    # check if query has more than one page
    pages=respData.body.div.find_all('a', {'href' : lambda x: x and x.startswith('Javascript:InnerLink_onchange')})

    if pages:
        npages = max(set(int(tag['href'].split(',')[-2]) for tag in pages))

        # skip first page, already retrieved
        currentpage=2

        while currentpage <= npages:

            # update page to retrieve in new query
            VARS['Page']=str(currentpage)

            # open and read url
            respData = requestURL(URL,VARS)

            if args.debug:
                print('processing query madhn=%s tafsirn=%s soran=%s ayan=%s pagen=%s' % (madhn,tafsirn,soran,ayan,VARS['Page']))

            # save html of query for page 1
            saveRetrivedURL(respData,madhn,tafsirn,soran,ayan,currentpage)

            sleep(uniform(SLEEP_MIN, SLEEP_MAX))

            # update npages in case there are more than expected in page 1 query
            # eg: query 1-1-2-233 indicates that there are: 1..10 مزيد pages, the total is 22, so npage should be update twice
            pages=respData.body.div.find_all('a', {'href' : lambda x: x and x.startswith('Javascript:InnerLink_onchange')})
            npages = max(set(int(tag['href'].split(',')[-2]) for tag in pages))

            currentpage+=1
