#!/usr/bin/env python
#
#     get_all_ayas.py
# 
# Get file with all aya texts to do checkings.
#
# usage:
#   $ python get_all_ayas.py | uniq > all_aya_texts.txt
#
#######################################################

import sys
import os
import re
import json
from argparse import ArgumentParser

from pprint import pprint #DEBUG

MYPATH = os.path.dirname(os.path.realpath(__file__))
INPUTPATH = os.path.join(MYPATH, '../data/all/merged')

OUT_VERSES = []

if __name__ == '__main__':

    parser = ArgumentParser(description='get all aya text and their indexes from data/all/merged/')
    parser.add_argument('--debug', action='store_true', help='debug mode')
    args = parser.parse_args()
    
    fnames = (f for f in os.listdir(INPUTPATH) if os.path.splitext(f)[1]=='.json')

    fn_obj = ((fn, json.load(open(os.path.join(INPUTPATH, fn)))) for fn in fnames)

    for fn,obj in fn_obj:

        if args.debug:
            print('**', fn, file=sys.stderr)

        ayagroup = obj['ayahtexts']
        try:
            isura, iaya_start, iaya_end = os.path.splitext(fn)[0].rsplit('-', 3)[1:]
        except:
            print('Error parsing filename "%s"' % fn, file=sys.stderr)
            sys.exit(1)

        for index,iaya in enumerate(range(int(iaya_start), int(iaya_end)+1)):
            OUT_VERSES.append((isura, iaya, re.sub(r'\s+', ' ', ayagroup[index])))

    for i,j,text in sorted(OUT_VERSES, key=lambda x: (int(x[0]),int(x[1]))):
        print(i, j, text, sep='|')

