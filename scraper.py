#!/usr/bin/python3.4
#
#     scraper.py
#
# parse and filter html downloads:
#   * parse all html files downloaded with crawler.py from altafsir.com
#   * group files of same query but different pages and extract all texts
#   * yield a filtered version into json file
#
#
# example:
#    $ python scraper.py ../../data/all/ori/ ../../data/all/filtered
#
# format of name of input file:
#
#   altafsir-i-j-k-l-m.html
#
#   * i: tMadhNo
#   * j: tTafsirNo
#   * k: tSoraNo
#   * l: Page
#   * m: timestamp in epoch format (this makes possible to keep track of unexpected changes in the original website)
#
#
#   INPUT structure of the html:
#   ---------------------------
#   <html>
#    <body>
#     <div class="container">
#      <div class="innerContent">
#       <div class="PageDisp" id="disp">
#        <div class="TextArabic" dir="RTL" id="DispFrame" ... >
#         <div ... > ... </div>
#         <div align="right" class="ArabicText" dir="rtl" ir="SearchResults" ... >
#          +----------------------------------------------------------------------
#          | <div align="right" class="ArabicText" dir="rtl" id="AyaMenu" ... > ... </div>
#          | <font class="TextAyah" id="AyahText" >
#          |  <a class="TextAyah" ... >
#          |   **AYAH_TEXT verse 1**
#          | </font>
#          |
#          | <div align="right" class="ArabicText" ... >
#          |  <font class="TextResultArabic">
#          |   <font color="black">
#          |    **PLAIN_COMMENTARY_TEXT**
#          |  </font>
#          |  
#          |  <font class="TextVerse">
#          |   <font color="Olive">
#          |    **VERSE_REF**
#          |
#          |  <font class="TextAyah">
#          |   **OTHER_AYAH_REF**
#          |  </font>
#          |
#          |  <font class="TextResultArabic">
#          |   <font color="Red">
#          |    **HADITH_REF**
#          |  [...]
#          | </div>
#          +----------------------------------------------------------------------
#         </div>
#
#   OUTPUT structure of the JSON format:
#   -----------------------------------
#   {
#     "ayahTexts"    : [ "..." ,                                    // ayahs being commented
#                        "..." ,
#                         ...    ]
#     "commentaries" : [ { "type" : "plain"  , "text" : "..." } ,   // commentary
#                        { "type" : "ayah"   , "text" : "..." } ,   // other quranic text reference
#                        { "type" : "hadith" , "text" : "..." } ,   // hadith reference
#                        { "type" : "verse"  , "text" : "..." } ,   // non-revelation references
#                           ...  ]
#   }
#                                                                                                                      يا كبيكج يا كبيكج يا كبيكج
########################################################################################## 

from argparse import ArgumentParser
from sys import argv,stdin,stdout,stderr,exit
from subprocess import Popen,PIPE
from os import path,listdir
from os.path import isfile,join
from bs4 import BeautifulSoup
from json import dumps

#
# parse args
#

parser = ArgumentParser(description='parse html infile and yiled a json file with relevant info')
parser.add_argument('original_dir', help='directory of html input files')
parser.add_argument('filtered_dir', help='directoy for json filtered files')
#parser.add_argument('merged_dir', help='directoy for json merged files') #DEPRECATED
parser.add_argument('--debug', action='store_true', help='print each query data to stderr')
args = parser.parse_args()

#
# constants
#

# current directory
PATH = path.dirname(path.realpath(argv[0]))

# merger process
#MERGER = path.join(PATH, 'merger.py')


#
# functions
#

def extractCommentaries(htmlobject, outdict):
    """Read and parse htmlobject, extract commentary texts and dump them into outdict."""
    
    # read input html
    soup = BeautifulSoup(htmlobject, 'html.parser')
    
    # selection that includes all texts to extract
    sel=soup.body.div.find('div', class_='PageDisp').find('div', id='DispFrame').find('div', id='SearchResults')

    # extract commentaries and interspersed quranic references
    comments=(sel.find_all('div')[1])
    
    for com in comments.find_all('font'):
        commentClass=com.get('class')
    
        if commentClass:
            tag=commentClass[0]
    
            if tag=='TextResultArabic':
                text=com.font.text.strip()
    
                if text:
                    
                    # text belonging to commentary itself, in black color
                    if com.font['color'] == 'black':
                        outdict['commentaries'].append({'type' : 'plain', 'text' : text})
                    
                    # text belonging to a reference to the hadith
                    if com.font['color'] == 'Red':
                        outdict['commentaries'].append({'type' : 'hadith', 'text' : text})

    
            # text belonging quranic reference (different from the ayah being commented,
            # that is always at the begining of the text, and it is extracted before), in ForestGreen color
            elif tag=='TextAyah':
                text=com.font.text.strip()
   
                if text:
                    outdict['commentaries'].append({'type' : 'ayah', 'text' : text})

            # text belonging to a non-quranic reference, typically a verse, in ForestGreen color
            elif tag=='TextVerse':
                text=com.font.text.strip()
   
                if text:
                    outdict['commentaries'].append({'type' : 'verse', 'text' : text})
    
            # Msg and MsgWord contain only page numbers, they are handled by crawler.py and updated in the filenames
            elif tag=='Msg' or tag=='MsgWord':
                pass

            else:
                print('WARNING: unexpected text inside commentary, class="%s"' % tag, file=stderr) 

    return  # explicit only to help visualization

#
# main
#

filegroups={}

# collect all files in directory within groups of the same query {index : [f1, f2, ...], ...}
for fname in filter(isfile, (join(args.original_dir,f) for f in listdir(args.original_dir))):

    try:
        pref,madhabn,tafsirn,suran,ayahn,page,rest = fname.split('-')
    except ValueError:
        print('ERROR: upexpected file "%s" in input directory "%s"' % (fname,args.original_dir), file=stderr)
        exit(1)

    # key of dict is tuple with all index info
    key = int(madhabn),int(tafsirn),int(suran),int(ayahn)

    # value of dict is list with all filenames corresponding to key
    filegroups[key] = filegroups.get(key,[])+[fname]


# loop each index and create one json outfile for each group of files (same query, different pages)
for index,filelist in filegroups.items():
    
    #create output format for new group
    out = {'ayahtexts' : [], 'commentaries': []}

    # sort files by page
    sortedFilelist = sorted(filelist, key=lambda x: int(x.split('-')[-2]))

    # get ayah text from first file
    with open(sortedFilelist[0]) as inf:

        # read input html
        soup = BeautifulSoup(inf, 'html.parser')
    
        # selection that includes all texts to extract
        sel=soup.body.div.find('div', class_='PageDisp').find('div', id='DispFrame').find('div', id='SearchResults')
    
        # select the ayah texts that the commentaries are refering
        ayaTexts=sel.find('font', id='AyahText')
    
        # extract all ayas
        for aya in ayaTexts.find_all(class_="TextAyah"):
            text=aya.text.strip()

            if text:
                out['ayahtexts'].append(text)

    # get commentary texts from all files
    for fname in sortedFilelist:

        if args.debug: print('processing input file "%s"' % fname)

        # open current file and store its info in out
        with open(fname) as inf:
            extractCommentaries(inf, out)

    # print output only if there are commentaries
    if out['commentaries']:

        # dump current out structure in json format file
        outfname = 'altafsir-%d-%d-%d-%d.json' % index

        if args.debug: print('processing output file "%s"' % outfname)

        with open(join(args.filtered_dir,outfname), 'w') as outf:
            print(dumps(out, ensure_ascii=False), file=outf) # if flag True, all non ascii chars are escaped


#DEPRECATED processes are better called independently
#call to merger to reorganize files and join the ones with the same content
#processMerger = Popen(('python %s %s %s ' % (MERGER, args.filtered_dir, args.merged_dir)).split(), stdin=PIPE, stdout=PIPE)
#processMerger.communicate()
