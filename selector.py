#!/usr/bin/python3.4
#
#     selector.py
#
# copies quranic commentaries that refer to a specific
# list of sura:aya references into new directory
#
# dependencies:
#   * util.py
#
# example:
#   $ python selector.py references.txt ../../data/all/final ../../data/prepared
#
#################################################################################

from sys import stdin
from os import path,listdir
from ntpath import basename
from json import load,dumps
from argparse import ArgumentParser,FileType
from util import loadReferences

#
# parse args
#

parser = ArgumentParser(description='select commentaries on specific sura:aya refs and copies them to dir')
parser.add_argument('reference_file', nargs='?', type=FileType('r'), default=stdin,
                                                 help='list of sura:aya pairs to select')
parser.add_argument('merged_dir', help='input directory with merged files')
parser.add_argument('output_dir', help='output directory for files selected')

args = parser.parse_args()

#
# main
#

# load references to select
refs = loadReferences(args.reference_file)

# get all filenames from input directory
fnames = filter(path.isfile, (path.join(args.merged_dir, f) for f in listdir(args.merged_dir)))


def match(isura, iaya_ini, iaya_end, references=refs):
    """Checks if the isura and range of iayas match a ref contained in references
    Args:
        isura (string): index indicating the number of sura.
        iaya_ini (string): index indicating the first aya commented.
        iaya_end (string): index indicating the last aya commented.
        references (set of 2-string tuples): list of sura:aya references to be matched against.
    Return:
        True if indexes given match a ref from references, False otherwise
    """
    refs_to_isura = filter(lambda x: x[0]==isura, refs)
    return any(filter(lambda x: x[1]>=iaya_ini and x[1]<=iaya_end, refs_to_isura))


#def match(isura, iaya_ini, iaya_end, references=refs):
#    """Checks if the isura and range of iayas match a ref contained in references
#    Args:
#        isura (string): index indicating the number of sura.
#        iaya_ini (string): index indicating the first aya commented.
#        iaya_end (string): index indicating the last aya commented.
#        references (set of 2-string tuples): list of sura:aya references to be matched against.
#    Return:
#        True if indexes given match a ref from references, False otherwise
#    """
#    for sura,aya in refs:
#        if sura==isura:
#            if aya >=iaya_ini and aya<=iaya_end:
#                return True
#    return False

# filter files getting only the ones matching the references
matched_fnames = filter(lambda fn: match(*path.splitext(basename(fn))[0].rsplit('-',3)[1:]), fnames)

# take base filename and json object from all files
copies = ((basename(fn),load(open(fn))) for fn in matched_fnames)

# create newfiles in output dir
for fname, content in copies:

    with open(path.join(args.output_dir, fname), 'w') as outf:

        # if flag True, all non ascii chars are escaped
        print(dumps(content, ensure_ascii=False), file=outf)

