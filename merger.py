#!/usr/bin/python3.4
#
#     merger.py
#
# parses all filtered files from a given directory, and merges the ones
# with identical content, then updates all aya ranges to fix inconsistencies
#
# dependencies:
#   gimme_verse_quran/gimme_verse_index.py
#
# example:
#    $ python merger.py ../../data/all/filtered ../../data/all/merged
#
# format of input fnames:
#
#     altafsir-i-j-k-l.json    i : imadhab
#                              j : itafsir
#                              k : isura
#                              l : iaya
#                                                                                يا كبيكج احفظ الرمز
# format of outpur fnames:
#
#     altafsir-i-j-k-l-m.json    i : imadhab
#                                j : itafsir
#                                k : isura
#                                l : iaya_ini
#                                m : iaya_end
#
########################################################################################################

from sys import argv,stderr,exit
from os import path,listdir
from ntpath import basename
from imp import load_source
from argparse import ArgumentParser
from itertools import groupby
from json import load,dumps
from pprint import pprint
from itertools import groupby
from pprint import pprint

#
# constants and relative paths
#

# current path
MYPATH = path.dirname(path.realpath(argv[0]))

# import function to get quranic reference of a given text
getIndexQuran = load_source('gimme_verse_index', path.join(MYPATH, 'gimme_verse_quran/gimme_verse_index.py')).getIndex

######################################################################################

#
# parse args
#

parser = ArgumentParser(description='merges filtered files to update aya ranges')
parser.add_argument('filtered_dir', help='input directory with filtered files to merge')
parser.add_argument('merged_dir', help='output directory for merged files')
args = parser.parse_args()

######################################################################################

#
# functions
#

def decideAyaNoRange(madhabNo, tafsirNo, suraNo, ayaNoMin, ayaNoMax, groupRefs):
    """Choose the corrent aya number from groupRefs that agrees with other data.

    Args:
        madhabNo (int): Number of madhab (for debugging).
        tafsirNo (int): NUmber of tafsir (for debugging).
        suraNo (int): Number of sura.
        ayaNoMin (int): Number of first ayah as states by the query.
        ayaNoMax (int): Number of last ayah as states by the query.
        groupRefs (tuple of 2-item tuples): Collection Groups of (sura,ayah) quranic references from a set of texts.

    Return:
        tuple(int,int): First int indicates iniAyaNo and second int endAyaNo.
    """

    # get ayas from references that match number of sura, eg.: if suraNo = 3 then,
    #       from ( ((2, 1), (3, 1), (29, 1), (30, 1), (3, 99)),    ((3, 2),) )
    #       get  [ [1, 99],  [2] ]
    groupsAyas = list(list(x[1] for x in gr if x[0]==suraNo) for gr in groupRefs)

    # check there are references left
    if not groupsAyas[0]:
        print('Error in ref %d,%d  Q(%d:%d-%d): Refs do not match sura.' % (madhabNo,tafsirNo,suraNo,ayaNoMin,ayaNoMax), file=stderr)
        exit(1)

    # if only one text aya
    if len(groupsAyas) == 1:

        # if text aya has only one index, return it, [[1]]
        if len(groupsAyas[0]) == 1:
            return (groupsAyas[0][0], groupsAyas[0][0])

        # if more, check if it matches ayaNoMin
        else:
            if ayaNoMin in groupsAyas[0]:
                return (ayaNoMin, ayaNoMin)

            else:
                print('Error in ref %d,%d  Q(%d:%d-%d): Impossible to know index.' %
                           (madhabNo,tafsirNo,suraNo,ayaNoMin,ayaNoMax), file=stderr)

    # separate first item from the rest, eg.: from  [ [57, 31], [58, 32], [59], [60] ]
    #                                         first = [57,31]   rest = [ [58, 32], [59], [60] ]
    first, rest = groupsAyas[0], groupsAyas[1:]

    # zip sequence of number counting from each number in "first": [ [(58, [58, 32]), (59, [59]), (60, [60])] ,
    #                                                                [(32, [58, 32]), (33, [59]), (34, [60])] ]
    possibleSeqs = list(list(enumerate(rest,n+1)) for n in first)    

    # remove those sequences which do not contain the count number: [ [(58, [58, 32]), (59, [59]), (60, [60])] ]
    filteredSeqs = list(filter(lambda seq: all((x[0] in x[1] for x in seq)), possibleSeqs))

    # get sequence of numbers from list: [ [58, 59, 60] ]
    resultedSeq = list(list(x[0] for x in seq) for seq in filteredSeqs)

    # no sequence found
    if not resultedSeq[0]:
        print('Error in ref %d,%d  Q(%d:%d-%d): No sequence found.' %
                           (madhabNo,tafsirNo,suraNo,ayaNoMin,ayaNoMax), file=stderr)

        print('==========================', ) #DEBUG
        print('groupRefs:                       ') ; pprint(groupRefs, indent=20)
        print('groupsAyas (coge ayas):          ') ; pprint(groupsAyas, indent=20)
        print('possibleSeqs (zip seq):          ') ; pprint(possibleSeqs, indent=20)
        print('filteredSeqs (quita las no seq): ') ; pprint(filteredSeqs, indent=20)
        print('resultedSeq (get only seq):      ') ; pprint(resultedSeq, indent=20)

        exit(1)

    # if more than one seq, reduce to range indicated to ones matching (ayaNoMin:ayaNoMax)
    if len(resultedSeq) > 1:
        resultedSeq = list(seq for seq in resultedSeq if seq[0] >= ayaNoMin and seq[-1] <= ayaNoMax)

    # if still more than one sequence, error
    if len(resultedSeq) > 1:

        print('Error in ref %d,%d  Q(%d:%d-%d): More than one sequence.' %
                           (madhabNo,tafsirNo,suraNo,ayaNoMin,ayaNoMax), file=stderr)

        print('==========================', ) #DEBUG
        print('groupRefs:                       ') ; pprint(groupRefs, indent=20)
        print('groupsAyas (coge ayas):          ') ; pprint(groupsAyas, indent=20)
        print('possibleSeqs (zip seq):          ') ; pprint(possibleSeqs, indent=20)
        print('filteredSeqs (quita las no seq): ') ; pprint(filteredSeqs, indent=20)
        print('resultedSeq (get only seq):      ') ; pprint(resultedSeq, indent=20)

        exit(1)
 
    #DEBUG
    #iniAya = resultedSeq[0][0]-1
    #endAya = resultedSeq[0][-1]
    #if iniAya != ayaNoMin or endAya != ayaNoMax:
    #    print('\n\n**********************************', ) #DEBUG
    #    print('groupRefs:                       ') ; pprint(groupRefs, indent=20)
    #    print('groupsAyas (coge ayas):          ') ; pprint(groupsAyas, indent=20)
    #    print('possibleSeqs (zip seq):          ') ; pprint(possibleSeqs, indent=20)
    #    print('filteredSeqs (quita las no seq): ') ; pprint(filteredSeqs, indent=20)
    #    print('resultedSeq (get only seq):      ') ; pprint(resultedSeq, indent=20)
    #    print('Ref %d,%d  Q(%d:%d-%d)' % (madhabNo,tafsirNo,suraNo,ayaNoMin,ayaNoMax))
    #    print('\nRETURN: ', (iniAya, endAya))

    # return updated range
    return (resultedSeq[0][0]-1, resultedSeq[0][-1])




######################################################################################

#
# main
#

# get all filenames
fnames = filter(path.isfile, (path.join(args.filtered_dir, f) for f in listdir(args.filtered_dir)))

# get (imadhab,itafsir,isura,iaya) and sort by it : ( ((i,j,k,l), fn), ... )
sorted_fnames = sorted((tuple(map(int,path.splitext(basename(fn))[0].rsplit('-',4)[1:])),fn) for fn in fnames)

# load json and group indexes by equal content : ( [ ((i,j,k,l), content), ... ], ... )
grouped_content = (list(g) for k,g in groupby(((ind,load(open(fn))) for ind,fn in sorted_fnames), key=lambda x: x[1]))

# group indexes : ((content, [(i,j,k,l), ...]), ... )
grouped_indexes =((g[0][1],list(ind[0] for ind in g)) for g in grouped_content)

# get range from indexes : ( (content, (i,j,k), min(l), max(l))), ... )
content_range = ((cont, ind[0][:3], min(i[3] for i in ind), max(i[3] for i in ind)) for cont,ind in grouped_indexes)

# get actual quran indexes from all texts : ( (content, (i,j,k), ayamin, ayamax, (((i,j),(i,j)), ((i,j)), ...)), ... )
content_indexes = ((cont, ind, imin, imax, tuple(tuple(getIndexQuran(txt, exact_script=False)) for txt in cont['ayahtexts']))
                                                                                 for cont,ind,imin,imax in content_range)

# decide the aya number for each group : ((content,(i,j,k),(j1,j2,j3))), ...)
content_refs = ((cont, ind, decideAyaNoRange(ind[0],ind[1],ind[2],imin,imax,refs))
                                           for cont,ind,imin,imax,refs in content_indexes)


# get first and last aya indexes and create new filename : (content,fname)
merged_fnames = ((cont, 'altafsir-%d-%d-%d-%d-%d.json' % (ind[0],ind[1],ind[2],arange[0],arange[1])) for cont,ind,arange in content_refs)

# print newfiles in outfolder
for content, newfname in merged_fnames:

    with open(path.join(args.merged_dir, newfname), 'w') as outf:

        # if flag True, all non ascii chars are escaped
        print(dumps(content, ensure_ascii=False), file=outf)
